window.addEventListener("DOMContentLoaded", async () => {

    const locationListUrl = "http://localhost:8000/api/locations/";
    const locationResponse = await fetch(locationListUrl);

    if (locationResponse.ok) {
        const locationData = await locationResponse.json();
        console.log(locationData)
        const locations = locationData.locations;
        const locationSelect = document.getElementById("location");

        for (let location of locations) {
            const option = document.createElement("option");
            option.innerHTML = location.name
            option.value = location.id
            locationSelect.appendChild(option)
        }
    }
})

const formTag = document.getElementById("create-conference-form");
formTag.addEventListener("submit", async (e) => {
    e.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            "Content-Type": "application/json",
        }

    }
    const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
    if (conferenceResponse.ok) {
        formTag.reset();
        const newConference = conferenceResponse.json()
        console.log(newConference)


    }
})
