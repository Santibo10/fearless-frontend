window.addEventListener('DOMContentLoaded', async () => {



    function createCard(name, description, pictureUrl, starts, ends, location) {
        return `
        <div class="col-4">
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            <p class="card-text"><small>${starts} - ${ends}</small></p>
        </div>
        </div>
        </div>
        `;
    }

    function alertError(problem) {
        return `<div class="alert alert-danger alert-dismissible fade show" role="alert">
    Sorry, ${problem}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`
    }

        const url = 'http://localhost:8000/api/conferences/';
        try {
            const response = await fetch(url);

            if (!response.ok) {
                const errorMessage = alertError("We could not fetch your conferences. Please try again later");
                const message = document.querySelector(".my-h2");
                message.innerHTML = errorMessage;
            } else {
                const data = await response.json();

                for (let conference of data.conferences) {

                    const detailUrl = `http://localhost:8000${conference.href}`;
                    const detailResponse = await fetch(detailUrl);
                    if (detailResponse.ok) {
                        const details = await detailResponse.json();
                        const title = details.conference.name;
                        const description = details.conference.description;
                        const pictureUrl = details.conference.location.picture_url;
                        const starts = details.conference.starts.split("T")[0]
                        const ends = details.conference.ends.split("T")[0]
                        // const starts = new Date(details.conference.starts).toDateString();
                        // const ends = new Date(details.conference.ends).toDateString();
                        const location = details.conference.location.name
                        const html = createCard(title, description, pictureUrl, starts, ends, location);
                        const column = document.querySelector(".row");
                        column.innerHTML += html
                    }
                }
            }
        } catch (e){
            const errorMessage = alertError("There was a problem loading the conferences please try again later.");
            const message = document.querySelector(".my-h2");
            message.innerHTML = errorMessage;
        }
});
