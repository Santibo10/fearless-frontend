// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload")
if (payloadCookie) {
  const payload = JSON.parse(atob(payloadCookie.value));
    console.log(payload.user.perms);

    const payloadPerms = payload.user.perms
    if (payloadPerms.includes("events.add_conference")) {
        const conferencePerm = document.getElementById("new-conference-link");
        conferencePerm.classList.toggle("d-none");
    }
    if (payloadPerms.includes("events.add_location")) {
        const locationPerm = document.getElementById("new-location-link");
        locationPerm.classList.toggle("d-none")
    }
    if (payloadPerms.includes("presentations.add_presentation")) {
        const presentationPerm = document.getElementById("new-presentation-link");
        presentationPerm.classList.toggle("d-none");
    }

}
