window.addEventListener("DOMContentLoaded", async () => {


    const statesUrl = "http://localhost:8000/api/states/";
    const statesResponse = await fetch(statesUrl);
    if (statesResponse.ok) {
        const statesData = await statesResponse.json();
        const stateList = statesData.states
        const stateDropDown = document.getElementById("state");

        for (let state of stateList) {
            const option = document.createElement("option");
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            stateDropDown.appendChild(option);
        }
    }
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit", async e => {
        e.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json)

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            "Content-Type": "application/json",
        },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json()
        console.log(newLocation)
        }
    });
});
