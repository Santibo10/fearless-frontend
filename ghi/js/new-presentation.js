window.addEventListener("DOMContentLoaded", async () => {

    const conferenceUrl = " http://localhost:8000/api/conferences/";
    const conResponse = await fetch(conferenceUrl);

    if (conResponse.ok) {
        const conData = await conResponse.json();
        const conferences = conData.conferences;
        const select = document.getElementById("conference");


        for (let conference of conferences) {
            const option = document.createElement("option");
            option.innerHTML = conference.name;
            option.value = conference.id;
            select.appendChild(option);
        }
    }
    const formTag = document.getElementById("create-presentation-form");
    formTag.addEventListener("submit", async (e) => {
        e.preventDefault()

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const select = document.getElementById("conference");

    const presentationUrl = `http://localhost:8000/api/conferences/${select.value}/presentations/`;
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            "Content-Type": "application/json"
        }
    }
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const responseData = await response.json();
        console.log(responseData);
    }
    })

})
