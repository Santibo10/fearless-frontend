window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }
    const spinner = document.getElementById("loading-conference-spinner");
    spinner.classList.toggle("d-none");
    const select = document.querySelector("select");
    select.classList.toggle("d-none");

    const formTag = document.getElementById("create-attendee-form");
    formTag.addEventListener("submit", async (e) => {
        e.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const attendeeUrl = "http://localhost:8001/api/attendees/";
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": "application/json",
            }
        };
        const attendResponse = await fetch(attendeeUrl, fetchConfig);
        if (attendResponse.ok) {
            const message = document.getElementById("success-message")
            message.classList.toggle("d-none")
            formTag.classList.toggle("d-none");
            const attendee = attendResponse.json();
            console.log(attendee)
        }

    })

  });
