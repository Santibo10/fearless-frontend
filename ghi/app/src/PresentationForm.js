import React, {useState, useEffect} from "react";

export default function PresentationForm() {

    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [companyName, setCompanyName] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conference, setConference] = useState("");

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const responseData = await response.json();
            setConferences(responseData.conferences);
        }
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {};
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;


        const locationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        console.log(locationUrl)
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const finalResponse = await response.json()
            console.log(finalResponse)

            setName("")
            setEmail("")
            setCompanyName("")
            setTitle("")
            setSynopsis("")
            setConference("")
        }
    }



    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="presenter_name" id="name" className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={email} onChange={handleEmailChange} placeholder="email" required type="email" name="presenter_email" id="email" className="form-control"/>
                <label htmlFor="email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input value={companyName} onChange={handleCompanyNameChange} placeholder="compant-name" type="text" name="company_name" id="company-name" className="form-control"/>
                <label htmlFor="company-name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={title} onChange={handleTitleChange} placeholder="title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea value={synopsis} onChange={handleSynopsisChange} placeholder="synopsis" required type="textarea" name="synopsis" id="synopsis" className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select value={conference} onChange={handleConferenceChange} required id="conference" name="conference" className="form-select">
                  <option value="">Choose a Conference</option>
                  {conferences.map(conference => {
                    return (
                        <option value={conference.id} key={conference.id}>
                        {conference.name}
                        </option>
                        )
                  })}

                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}
