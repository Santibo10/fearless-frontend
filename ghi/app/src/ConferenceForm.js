import { useEffect, useState } from "react";

export default function ConferenceForm() {

    const [locations, setLocations] = useState([]);
    const [name, setName] = useState("");
    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [description, setDescription] = useState("");
    const [maxPres, setMaxPres] = useState("");
    const [maxAt, setMaxAt] = useState("");
    const [location, setLocation] = useState("");


    const fetchLocationData = async () => {
        const locationListUrl = "http://localhost:8000/api/locations/";
        const locationResponse = await fetch(locationListUrl);

        if (locationResponse.ok) {
            const locationData = await locationResponse.json();
            setLocations(locationData.locations)
        }
    }

    const handleNameChange = (event) => {
        const value=event.target.value;
        setName(value);
    }
    const handleStartDateChange = (event) => {
        const value=event.target.value;
        setStartDate(value);
    }
    const handleEndDateChange = (event) => {
        const value=event.target.value;
        setEndDate(value);
    }
    const handleDescriptionChange = (event) => {
        const value=event.target.value;
        setDescription(value);
    }
    const handleMaxPresChange = (event) => {
        const value=event.target.value;
        setMaxPres(value);
    }
    const handleMaxAtChange = (event) => {
        const value=event.target.value;
        setMaxAt(value);
    }
    const handleLocationChange = (event) => {
        const value=event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPres;
        data.max_attendees = maxAt
        data.location = location

        console.log(data)

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }

        }
        const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
        if (conferenceResponse.ok) {
          const response = await conferenceResponse.json()
          console.log(response)

          setName("")
          setStartDate("")
          setEndDate("")
          setDescription("")
          setMaxPres("")
          setMaxAt("")
          setLocation("")
        }


    }


    useEffect(() => {
        fetchLocationData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={startDate} onChange={handleStartDateChange} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input value={endDate} onChange={handleEndDateChange} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">End Date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} placeholder="Description" required type="textarea" name="description" id="description" className="form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={maxPres} onChange={handleMaxPresChange} placeholder="Maximum Presentations" required type="number" name="max_presentations" id="presentations" className="form-control"/>
                <label htmlFor="presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxAt} onChange={handleMaxAtChange} placeholder="Maximum Attendeess" required type="number" name="max_attendees" id="attendees" className="form-control"/>
                <label htmlFor="attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option value={location.id} key={location.href}>
                                {location.name}
                            </option>
                        )

                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
