import { useState, useEffect } from "react";
import logo from "./logo.svg";

export default function AttendConference() {

    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [submitted, setSubmitted] = useState(false)

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {};
        data.name = name;
        data.email = email;
        data.conference = conference

        const attendeeUrl = "http://localhost:8001/api/attendees/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };
        const attendResponse = await fetch(attendeeUrl, fetchConfig);
        if (attendResponse.ok) {
            const response = await attendResponse.json()
            console.log(response)

            setConference("")
            setName("")
            setEmail("")
            setSubmitted(true)
        }
    }


    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className="my-5">
        <div className="row">
          <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src={logo}/>
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <div className={submitted ? "alert alert-success mb-0" : "alert alert-success d-none mb-0"} id="success-message">
                  Congratulations! You're all signed up!
                </div>
                <form className={submitted ? "d-none" : ""} onSubmit={handleSubmit} id="create-attendee-form">
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">
                    Please choose which conference
                    you'd like to attend.
                  </p>
                  <div className={conferences.length > 0 ? "d-flex justify-content-center mb-3 d-none" : "d-flex justify-content-center mb-3"} id="loading-conference-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div>
                </div>
                <div className="mb-3">
                    <select onChange={handleConferenceChange} value={conference} className={conferences.length > 0 ? "form-select" : "form-select d-none"} name="conference" id="conference" required>
                        <option value="">Select a conference</option>
                        {conferences.map( conference => {
                            return (
                                <option value={conference.href} key={conference.id}>
                                    {conference.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                  <p className="mb-3">
                    Now, tell us about yourself.
                  </p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input value={name} onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input value={email} onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}
